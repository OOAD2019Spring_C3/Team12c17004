

interface WeaponBehavior {
public void useWeapon();
}

 class SwordBehavior implements WeaponBehavior {
@Override
public void useWeapon() {
	// TODO Auto-generated method stub
	System.out.println("Swinging a sword");
}
}
 class KnifeBehavior implements WeaponBehavior{
	 @Override
	 public void useWeapon() {
	 	// TODO Auto-generated method stub
	 System.out.println("Cutting with a knife");	
	 }
	 }
 class BowAndArrowBehavior implements WeaponBehavior{
@Override
public void useWeapon() {
	// TODO Auto-generated method stub
System.out.println("shooting with bow and arrows");	
}
}
class AxeBehavior implements WeaponBehavior{
	 @Override
	 public void useWeapon() {
	 	// TODO Auto-generated method stub
	 System.out.println("dropping with an axe");	
	 }
	 }

abstract class Character {
	 private WeaponBehavior weapon;
	    Character(){
	        weapon = new SwordBehavior();
	    }
	    public void setWeapon(WeaponBehavior w){
	        this.weapon = w;
	    }
	    public void fight(){
	        System.out.print("fight!  ");
	        weapon.useWeapon();
	    }
}


class King extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Knight extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Queen extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Troll extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		King q  = new King();
		q.fight();
		q.setWeapon(new BowAndArrowBehavior());
		q.fight();
	}

}
