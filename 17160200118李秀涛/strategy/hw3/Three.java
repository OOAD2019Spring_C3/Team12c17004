package One;

interface Discount {
	double calculate(double price);
	}
 class Book {
	double price;
	Discount discount;

	void setPrice(double price) {
		this.price=price;
	}
	void setDiscount(Discount discount) {
		this.discount=discount;
	};
	double getPrice() {
	return discount.calculate(this.price);
				}
	}

 class ComputerBook implements Discount{
	@Override
public  double calculate(double price) {
		return 0.9*price;
	}
	}

 class NovelBook implements Discount{
	@Override
	public double calculate(double price) {
		// TODO Auto-generated method stub
		price=price-10*(int)(price/100);
		return price;
	}
	}

class LanguageBook implements Discount {
	@Override
	public double calculate(double price) {
		// TODO Auto-generated method stub
		return price-2;
	}
	}


public class Three {

	public Three() {
		Book a = new Book();
		a.setPrice(50);
		a.setDiscount(new LanguageBook());
		System.out.println("语言类书籍优惠2元："+a.getPrice());
		a.setDiscount(new NovelBook());
		System.out.println("新闻类书籍满100减10:  "+a.getPrice());
		a.setDiscount(new ComputerBook());
		System.out.println("计算机类书籍9折:"+a.getPrice());
	}
	}

