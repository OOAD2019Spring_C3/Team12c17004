package Obesrve;

import java.util.ArrayList;
import java.util.List;


interface Member{
	void action();
}
interface Observer{
	void update();
	void setSubject(Subject sub); 

}
class Subject{
	 private List<Observer> list = new ArrayList<Observer>();
	
	 public void registerObserver(Observer observer) {
	     observer.setSubject(this);
		 list.add(observer);
	     System.out.println("Attached an observer"); 
	 }
	 public void deleteObserver(Observer observer) {
		 list.remove(observer);
	 }
	 public void nodifyObservers() {
	       for(Observer observer : list){
	        observer.update();
	 }	
}
	public void notifyObservers(Person person) {

		
	}
}
class Person implements Member, Observer{
    private String name;
    private Subject subject;

    public Person(String name){
        this.name = name;
    }
    public void action() {
        subject.notifyObservers(this);
    }
    public void update() {
        System.out.println(name + "收到，即将前往支援！");
    }
    public void setSubject(Subject sub){
        this.subject = sub;
    }
    public void beat(){
        System.out.println("我是" + name + ",正受到攻击，请求支援！");
        action();
    }
}

public class hw1 {

	public static void main(String[] args) {
		 Person p1 = new Person("1");
	     Person p2 = new Person("2");
	     Person p3 = new Person("3");


	        Subject group = new Subject();
	        group.register(p1);
	        group.register(p2);
	        group.register(p3);
	        p1.beat();
	     
         
	}
}

