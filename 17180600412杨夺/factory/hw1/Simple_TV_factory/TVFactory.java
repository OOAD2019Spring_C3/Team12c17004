package Simple_TV_factory;

import Simple_TV_factory.Simple_TV_factory.TV;

public  class TVFactory{
	public TV produceTV(String band){
		 TV tv=null;
		 
		if(band=="Haier"){
			tv=new HaierTV();
			tv.play();
		}
		else if(band=="Hisense"){
			tv=new HisenseTV();	
			tv.play();
		}
		return  tv;
		}
	}